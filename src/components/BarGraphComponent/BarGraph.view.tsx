import React, { useEffect } from "react";
import { FC } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { getCovidInformation } from "../../redux/actions";
import {
  getInfected,
  getRecovered,
  getQuarantined,
} from "../../redux/selectors";

export const BarGraphComponent: FC = () => {
  const infected = useSelector(getInfected());
  const recovered = useSelector(getRecovered());
  const quarantined = useSelector(getQuarantined());

  const data = [
    { name: "infected", value: infected },
    { name: "recovered", value: recovered },
    { name: "quarantined", value: quarantined },
  ];

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCovidInformation());
  }, [dispatch]);

  return (
    <>
      <h3>Bar Graph</h3>
      <BarChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
        barSize={20}
      >
        <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10 }} />
        <YAxis />
        <Tooltip />
        <Legend />
        <CartesianGrid strokeDasharray="3 3" />
        <Bar dataKey="value" fill="#8884d8" background={{ fill: "#eee" }} />
      </BarChart>
    </>
  );
};
