import "@/styles/index.scss";
import { CreateTemplate, meta } from "../storybook/utils";
import { BarGraphComponent as component } from "./BarGraph.view";

export default meta({
  title: "Components/BarGraph",
  component,
});

const Template = CreateTemplate(component);

export const BarGraphComponent = Template.bind({});
BarGraphComponent.args = {};
