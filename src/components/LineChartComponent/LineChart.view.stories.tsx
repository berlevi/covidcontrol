import "@/styles/index.scss";
import { CreateTemplate, meta } from "../storybook/utils";
import { LineChartComponent as component } from "./LineChart.view";

export default meta({
  title: "Components/LineChart",
  component,
});

const Template = CreateTemplate(component);

export const LineChartComponent = Template.bind({});
LineChartComponent.args = {};
