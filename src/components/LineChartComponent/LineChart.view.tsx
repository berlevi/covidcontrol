import React, { useEffect } from "react";
import { FC } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { getCovidInformation } from "../../redux/actions";
import {
  getInfected,
  getRecovered,
  getQuarantined,
} from "../../redux/selectors";

export const LineChartComponent: FC = () => {
  const infected = useSelector(getInfected());
  const recovered = useSelector(getRecovered());
  const quarantined = useSelector(getQuarantined());

  const data = [
    { name: "infected", value: infected },
    { name: "recovered", value: recovered },
    { name: "quarantined", value: quarantined },
  ];

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCovidInformation());
  }, [dispatch]);

  return (
    <>
      <h3>Line Chart</h3>
      <LineChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="pv"
          stroke="#8884d8"
          activeDot={{ r: 8 }}
        />
        <Line type="monotone" dataKey="value" stroke="#82ca9d" />
      </LineChart>
    </>
  );
};
