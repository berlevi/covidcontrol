import React, { useEffect } from "react";
import { FC } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Pie, PieChart } from "recharts";
import { getCovidInformation } from "../../redux/actions";
import {
  getInfected,
  getRecovered,
  getQuarantined,
} from "../../redux/selectors";

export const PieChartComponent: FC = () => {
  const infected = useSelector(getInfected());
  const recovered = useSelector(getRecovered());
  const quarantined = useSelector(getQuarantined());

  const data = [
    { name: "infected", value: infected },
    { name: "recovered", value: recovered },
    { name: "quarantined", value: quarantined },
  ];

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCovidInformation());
  }, [dispatch]);

  return (
    <>
      <h3>Pie Chart</h3>
      <PieChart width={400} height={400}>
        <Pie
          dataKey="value"
          isAnimationActive={false}
          data={data}
          cx="50%"
          cy="50%"
          outerRadius={80}
          fill="#8884d8"
          label
        />
      </PieChart>
    </>
  );
};
