import "@/styles/index.scss";
import { CreateTemplate, meta } from "../storybook/utils";
import { PieChartComponent as component } from "./PieChart.view";

export default meta({
  title: "Components/PieChart",
  component,
});

const Template = CreateTemplate(component);

export const PieChartComponent = Template.bind({});
PieChartComponent.args = {};
