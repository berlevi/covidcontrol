import React from "react";
import SiteLayout from "./layouts/SiteLayout";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="container">
      <SiteLayout></SiteLayout>
    </div>
  );
}

export default App;
