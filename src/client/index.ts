const header = <H extends {}>(header: H = {} as H) => ({
  "Content-type": "application/json",
  ...header,
});

//Fetch client
const fetchRequest = async <Res>(url: string) => {
  const res = fetch(url, {
    method: "GET",
    headers: header(),
    cache: "no-cache",
  });
  return (await res).json() as Promise<Res>;
};

export const getCovidData = () =>
  fetchRequest<{
    infected: number;
    activeInfected: number;
    deceased: number;
    recovered: number;
    quarantined: number;
    tested: number;
    sourceUrl: string;
    lastUpdatedAtSource: Date;
    lastUpdatedAtApify: Date;
    readMe: string;
  }>(
    //use the covid history link instead: https://api.apify.com/v2/datasets/Gm6qjTgGqxkEZTkuJ/items?format=json&clean=1
    "https://api.apify.com/v2/key-value-stores/RGEUeKe60NjU16Edo/records/LATEST?disableRedirect=true?token=LWE3QR6AXbQoHh7iYFoXryzQu"
  );
