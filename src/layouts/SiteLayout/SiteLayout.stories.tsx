import { CreateTemplate, meta } from "../../components/storybook/utils";
import { SiteLayout as component } from "./SiteLayout.view";

export default meta({
  title: "Components/SiteLayout",
  component,
});

const Template = CreateTemplate(component);

export const SiteLayoutComponent = Template.bind({});
SiteLayoutComponent.args = {};
