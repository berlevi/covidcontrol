import { FC, ReactNode } from "react";
import PieChartComponent from "../../components/PieChartComponent";
import LineChartComponent from "../../components/LineChartComponent";
import BarGraphComponent from "../../components/BarGraphComponent";

export const SiteLayout: FC = () => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-md-12 mt-5">
        <div className="row">
          <div className="col-md-12">
            <h3 className="text-center">Covid statistics in Hungary</h3>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <PieChartComponent />
          </div>
          <div className="col-md-4"></div>
        </div>
        <div className="row mt-4">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <BarGraphComponent />
          </div>
          <div className="col-md-4"></div>
        </div>
        <div className="row mt-4">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <LineChartComponent />
          </div>
          <div className="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
);
