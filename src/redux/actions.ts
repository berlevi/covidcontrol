import { getCovidData } from "../client";
import { IGetCovidInformation } from "./reducer";

export const getCovidInformation = async (): Promise<IGetCovidInformation> => ({
  type: "GET-COVID-INFORMATION",
  payload: {
    infected: (await getCovidData()).infected,
    recovered: (await getCovidData()).recovered,
    quarantined: (await getCovidData()).quarantined,
  },
});
