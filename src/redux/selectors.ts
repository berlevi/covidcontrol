import { Store } from "./store";

export const getInfected = () => (store: Store) => store.infected;
export const getRecovered = () => (store: Store) => store.recovered;
export const getQuarantined = () => (store: Store) => store.quarantined;
