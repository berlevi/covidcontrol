import { Action } from "redux";
import { Store, initStore } from "./store";

export interface IGetCovidInformation extends Action<"GET-COVID-INFORMATION"> {
  payload: {
    infected: number;
    recovered: number;
    quarantined: number;
  };
}

const getCovidReducer = (
  store: Store,
  { payload: { infected, recovered, quarantined } }: IGetCovidInformation
): Store => {
  return {
    ...store,
    infected,
    recovered,
    quarantined,
  };
};

export const reducer = (store: Store = initStore, action: Action) => {
  switch (action.type) {
    case "GET-COVID-INFORMATION":
      return getCovidReducer(store, action as IGetCovidInformation);
    default:
      return store;
  }
};
