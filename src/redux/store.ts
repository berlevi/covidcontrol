import { createStore, applyMiddleware } from "redux";
import { reducer } from "./reducer";
import { asyncMiddleware } from "./asyncMiddlewaer";

export const initStore = {
  infected: 0,
  recovered: 0,
  quarantined: 0,
};

export type Store = typeof initStore;

export const store = createStore(reducer, applyMiddleware(asyncMiddleware));
